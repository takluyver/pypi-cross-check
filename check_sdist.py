import argparse
from hashlib import sha256
import os
import os.path as osp
from pathlib import Path
import re
import requests
from requests_download import download
from subprocess import run, CalledProcessError, PIPE
import sys
import tarfile
from tempfile import TemporaryDirectory
from urllib.parse import urlparse

def candidate_urls(package_info):
    if hp := package_info['home_page']:
        yield hp

    yield from package_info['project_urls'].values()

def likely_repo_url(url):
    parsed = urlparse(url)
    if parsed.netloc.endswith(('github.com', 'gitlab.com', 'bitbucket.org')):
        return True
    return False

def unwrap_single(collection, item_desc='object'):
    """Ensure that collection contains exactly 1 item, and return it"""
    if len(collection) > 1:
        raise ValueError(f"Multiple {item_desc} found: {collection}")
    elif len(collection) < 1:
        raise ValueError(f"No {item_desc} found")
    return next(iter(collection))

def find_repo_url(package_info):
    urls = {u for u in candidate_urls(package_info) if likely_repo_url(u)}
    return unwrap_single(urls, "repo URL")

def find_tag(repo_dir, version):
    def list_tags(pattern):
        res = run(
            ['git', 'tag', '--list', pattern],
            cwd=repo_dir, stdout=PIPE, check=True, text=True,
        )
        return res.stdout.splitlines(keepends=False)

    if tags := list_tags(version):
        return unwrap_single(tags, 'VCS tag')  # tag == version

    tags = list_tags(f'*{version}*')
    # Exclude matches with numbers or . just before the version, and only
    # allow a suffix comprising '.' and '0'.
    # So with version='1.2', allow: 'rel-1.2', 'v1.2', '1.2.0',
    # but discard '11.2', '3.1.2', '1.2.1'.
    version_pat = r'(?<![\d\.])' + re.escape(version) + r'[.0]+$'
    if tags := [t for t in tags if re.search(version_pat, t)]:
        return unwrap_single(tags, 'VCS tag')

    if version.endswith('.0'):
        return find_tag(repo_dir, version[:-2])


def check_sdist(
        package, version=None, *, repo=None, tag=None, inspect_when=2,
):
    r = requests.get(f"https://pypi.org/pypi/{package}/json")
    r.raise_for_status()
    rj = r.json()
    if repo is None:
        repo = find_repo_url(rj['info'])
    elif osp.isdir(repo):
        repo = Path(repo).resolve()
    print("VCS repo:", repo)

    if version is None:
        version = rj['info']['version']

    sdist_info = [fi for fi in rj['releases'][version]
                    if fi['packagetype'] == 'sdist'][0]

    td = TemporaryDirectory()
    os.chdir(td.name)
    print("Working in", td.name)

    sdist_file = osp.basename(sdist_info['url'])
    download(sdist_info['url'], sdist_file)
    print("Downloaded sdist", sdist_file)

    extract = Path('sdist')
    extract.mkdir()

    with tarfile.open(sdist_file) as tf:
        # TODO: check extracted files don't go outside the target folder
        tf.extractall(extract)

    sdist_lvl0 = list(extract.iterdir())
    assert len(sdist_lvl0) == 1, sdist_lvl0
    sdist_folder = sdist_lvl0[0]
    print("Extracted sdist to:", sdist_folder)

    if isinstance(repo, Path):
        local_repo = repo
    else:
        local_repo = Path(td.name, 'git-repo')
        run(['git', 'clone', '--mirror', repo, local_repo], check=True)

    if tag is None:
        tag = find_tag(local_repo, version)
    if tag is None:
        raise ValueError(f"Couldn't find tag for version {version}")

    git_checkout = Path(td.name, 'git_checkout')
    run(['git', '-c', 'advice.detachedHead=', 'clone', '-b', tag, local_repo, git_checkout], check=True)
    print(f"Cloned tag {tag} as {git_checkout}")

    compare_dirs(git_checkout, sdist_folder, inspect_when=inspect_when)
    td.cleanup()

def compare_dirs(from_vcs: Path, from_sdist: Path, inspect_when=2):
    """Summarise different files, except files only in VCS"""
    added, changed = set(), set()
    for sdist_path in from_sdist.rglob('*'):
        if not sdist_path.is_file():
            continue

        rel_path = sdist_path.relative_to(from_sdist)
        vcs_path = (from_vcs / rel_path)
        if not vcs_path.is_file():
            added.add(rel_path)

        elif not files_identical(sdist_path, vcs_path):
            changed.add(rel_path)

    # This metadata file is generated for the sdist - ignore it
    added.discard(Path('PKG-INFO'))

    if changed:
        print(f"\n{len(changed)} files changed:")
        for p in sorted(changed):
            print(" ", p)
    if added:
        print(f"\n{len(added)} files added:")
        for p in sorted(added):
            print(" ", p)

    if changed:
        inspect_lvl = 2
    elif (changed or added):
        inspect_lvl = 1
    else:
        inspect_lvl = 0
    if inspect_lvl >= 2:
        run(['meld', from_vcs, from_sdist])

inspect_levels = {
    'always': 0,
    'if-changed-or-added': 1,
    'if-changed': 2,
    'never': 3,
}

def files_identical(f1: Path, f2: Path):
    if f1.stat().st_size != f2.stat().st_size:
        return False

    return hash_file(f1).digest() == hash_file(f2).digest()

def hash_file(path: Path):
    h = sha256()
    with path.open('rb') as f:
        while True:
            if not (b := f.read(4096)):
                return h
            h.update(b)

def main(argv=None):
    ap = argparse.ArgumentParser()
    ap.add_argument('package', help='Name of a package on PyPI')
    ap.add_argument('--pkg-version', help='Version to examine (default latest)')
    ap.add_argument('--repo', help='URL or path of a version control repo')
    ap.add_argument('--tag', help='VCS tag for version (default: try to guess)')
    ap.add_argument(
        '--inspect', choices=inspect_levels.keys(), default='if-changed',
        help='When to launch a diff tool to compare the folders manually',
    )
    args = ap.parse_args(argv)

    try:
        check_sdist(args.package, args.pkg_version, repo=args.repo, tag=args.tag,
                    inspect_when=inspect_levels[args.inspect])
    except CalledProcessError as e:
        print(e, file=sys.stderr)
        return 1
    
if __name__ == '__main__':
    sys.exit(main())
